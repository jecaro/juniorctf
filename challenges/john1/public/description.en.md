Title: John 1
Category: Passwords
Score: 50
Description:

Palpaguin has just finished the Death Star.

Rebels fortunately managed to get a brief access to the Death Star's control system. They copied an important file which contains the *hashed* password of Palpaguin's account. That's not exactly the password in itself, but it is helpful.
Also, they know that Palpaguin's password is a *fruit*, in lowercase.

Can you help us log into Palpaguin's account?
You can download the hashed password at the end of this description.

## Access Death Star's control system

To access the control system of the Death Star, enter: `ssh -p 2022 palpaguin@IP-ADDRESS`

Explanation:

- ssh is a real network protocol we use very frequently
- `-p 2022` means to use port number 2022. A port is a communication channel. Let's say it is a specific door at home, to enter a room. Except here, it's on a computer ;)
- `palpaguin` is the user name. He's called palpaguin, right?
- `192.168.0.8` is the IP address of the server. Your home's got an address, so have computers.


If it answers this, just reply yes:
```
The authenticity of host '[127.0.0.1]:2022 ([127.0.0.1]:2022)' can't be established.
RSA key fingerprint is 2d:93:f1:87:f8:91:8f:87:46:63:72:fd:c9:24:ed:d9.
Are you sure you want to continue connecting (yes/no)?
```

Then the system will ask for a password. That's the password we want you to find.

```
palpaguin@192.168.0.8's password: 
```

## Beware

This is a game. In "real life", accessing the account of another user is strictly forbidden. You should never do it, even just to have fun, or impress friends. Breaking passwords is not authorized either.

Sometimes, though, this is helpful. Imagine you do not remember your own password on your own computer! Of course, then, you are authorized to break your own password to access your own host!

## Break passwords with John The Ripper

To solve this challenge, I suggess you use a real and helpful tool named John The Ripper.

1. Install the tool. In a terminal, enter `sudo apt-get install john`. If that does not work, ask your parents for help.
2. Create a file, and write down all fruit names you know. One per line. Then save the file.
3. John The Ripper can test passwords for you. To do so, in a terminal, launch `john --wordlist=yourfile hashedpass`. Of course, replace yourfile with the name of your fruit list, and replace hashedpass with the file you downloaded and which contains the hashed password of Palpaguin.

