FROM alpine:3.7

ENV USER pico
ENV USER_PASSWORD 4maserati8

RUN apk update && apk upgrade && apk add bash openssh &&  rm -rf /var/cache/apk/* /tmp/*

RUN adduser -S $USER
RUN echo "${USER}:${USER_PASSWORD}" | chpasswd

# make sure we get fresh keys
RUN rm -rf /etc/ssh/ssh_host_rsa_key /etc/ssh/ssh_host_dsa_key
RUN ssh-keygen -f /etc/ssh/ssh_host_rsa_key -N '' -t rsa
RUN ssh-keygen -f /etc/ssh/ssh_host_dsa_key -N '' -t dsa
RUN mkdir -p /var/run/sshd

# Flag
ADD motd /etc/motd

EXPOSE 22
CMD ["/usr/sbin/sshd", "-D" ]
